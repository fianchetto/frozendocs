#DroneCode

Drone Mode Activated *(main loop starts)*

###*Follow Path*

####Identify Nearest Target

#####If Enemy 

* Go to Attack loop
 

#####If Friendly

* Loop back to *Follow Path*

###Attack Loop
#####Get coordinates to aim at when shooting a player

1. estimate how long it will take shot to hit target
2. calc position of target at that point of time
3. jump to 1., using projected position, loop until result is stable

#####If under attack

* Evade and Return to Attack Loop

#####Else 

* Attack Loop

#####If Drone Destroyed

* End

#####If Target Destroyed

* Loop back to *Follow Path*